-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 22, 2021 at 04:36 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aipsming_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `ak_cart`
--

DROP TABLE IF EXISTS `ak_cart`;
CREATE TABLE IF NOT EXISTS `ak_cart` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) DEFAULT NULL,
  `product_id` int(255) NOT NULL,
  `quantity` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ak_front_users`
--

DROP TABLE IF EXISTS `ak_front_users`;
CREATE TABLE IF NOT EXISTS `ak_front_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `customer` varchar(255) NOT NULL,
  `business_type` varchar(255) NOT NULL,
  `shipping_address` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ak_front_users`
--

INSERT INTO `ak_front_users` (`id`, `order_id`, `name`, `email`, `department`, `customer`, `business_type`, `shipping_address`, `created_at`) VALUES
(69, 491027487, 'vipul', 'vipul@gmail.com', 'sales', 'good', 'dealer', '31 surat', '2021-05-21 23:05:46');

-- --------------------------------------------------------

--
-- Table structure for table `ak_orders`
--

DROP TABLE IF EXISTS `ak_orders`;
CREATE TABLE IF NOT EXISTS `ak_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ak_orders`
--

INSERT INTO `ak_orders` (`id`, `order_id`, `products_id`, `created_at`) VALUES
(9, 12, 68, '2021-05-22'),
(8, 12, 2, '2021-05-22'),
(7, 12, 74, '2021-05-22');

-- --------------------------------------------------------

--
-- Table structure for table `ak_posts`
--

DROP TABLE IF EXISTS `ak_posts`;
CREATE TABLE IF NOT EXISTS `ak_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_title` varchar(255) NOT NULL,
  `post_name` varchar(255) NOT NULL,
  `post_description` varchar(255) NOT NULL,
  `post_image` varchar(255) DEFAULT NULL,
  `external_url` varchar(255) DEFAULT NULL,
  `post_type` varchar(255) NOT NULL,
  `download_file` varchar(255) DEFAULT NULL,
  `video_embed_url` varchar(255) DEFAULT NULL,
  `sort_order` int(10) UNSIGNED DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ak_posts`
--

INSERT INTO `ak_posts` (`id`, `post_title`, `post_name`, `post_description`, `post_image`, `external_url`, `post_type`, `download_file`, `video_embed_url`, `sort_order`, `parent_id`, `created`) VALUES
(1, 'Letterhead', 'letterhead', 'company letterhead for both US and Canada', 'letterhead.png', '', 'folder', '', NULL, NULL, 0, '0000-00-00 00:00:00'),
(2, 'Logos', 'logos', 'all logos for brands and company', 'logos-image.png', '', 'folder', '', NULL, NULL, 0, '0000-00-00 00:00:00'),
(3, 'America', 'america', 'Letterhead for US brands and facilities', 'america.jpg', '', 'folder', '', NULL, NULL, 1, '0000-00-00 00:00:00'),
(12, 'PowerPoint', 'powerpoint', 'most up to date powerpoint versions', '1614327540.png', '', 'folder', NULL, NULL, NULL, 0, '0000-00-00 00:00:00'),
(13, 'Business Cards', 'business-cards', 'Business cards for all employees', '1614327587.png', 'https://us287.agstorefront.com/uStore/login.aspx?Action=SessionExpired&StoreId=8&ReturnUrl=%2fuStore%2fHome', 'folder', NULL, NULL, NULL, 0, '0000-00-00 00:00:00'),
(14, 'Training Videos', 'training-videos', 'Salesforce teams and internal training videos', '1614327638.jpg', '', 'folder', NULL, NULL, NULL, 0, '0000-00-00 00:00:00'),
(15, 'Promotional Videos', 'promotional-videos', 'External Product videos for use by customers', '1614327680.jpg', '', 'folder', NULL, NULL, NULL, 0, '0000-00-00 00:00:00'),
(16, 'Product Pictures', 'product-pictures', 'Product Pictures for customers', '1614327733.jpg', 'https://apollovalves.smugmug.com/Product-Images', 'folder', NULL, NULL, NULL, 0, '0000-00-00 00:00:00'),
(17, 'Literature Request', 'literature-request', 'Literature website for ordering literature', '1614327787.jpg', 'http://apollovalves.com/literature', 'folder', NULL, NULL, NULL, 0, '0000-00-00 00:00:00'),
(18, 'Canada', 'canada', 'Letterhead for Canada', '1614340209.jpg', '', 'folder', NULL, NULL, NULL, 1, '0000-00-00 00:00:00'),
(19, 'Facilities', 'facilities', 'Letterhead for US plants', '1614340930.png', '', 'folder', NULL, NULL, NULL, 3, '0000-00-00 00:00:00'),
(20, 'Aalberts AIPS Americas letterhead', 'aalberts-aips-americas-letterhead', 'Aalberts AIPS Americas letterhead', '1614341104.png', NULL, 'file', '1614341104.docx', NULL, NULL, 3, '0000-00-00 00:00:00'),
(21, 'ApolloValves Conway2020', 'apollovalves-conway2020', 'ApolloValves Conway2020', 'ApolloValves Conway2020-1614344718.png', NULL, 'file', 'ApolloValves Conway2020-1614344718.docx', NULL, NULL, 19, '0000-00-00 00:00:00'),
(22, 'ApolloValves Pageland2020', 'apollovalves-pageland2020', 'ApolloValves Pageland2020', 'ApolloValves Pageland2020-1614344892.png', NULL, 'file', 'ApolloValves Pageland2020-1614344892.docx', NULL, NULL, 19, '0000-00-00 00:00:00'),
(23, 'Aalberts integrated piping Systems', 'aalberts-integrated-piping-systems', 'corporate Company logo', 'Aalberts integrated piping Systems-1614345194.png', '', 'folder', NULL, NULL, NULL, 2, '0000-00-00 00:00:00'),
(24, 'Brands', 'brands', 'all brand logos', 'Brands-1614345239.png', '', 'folder', NULL, NULL, NULL, 2, '0000-00-00 00:00:00'),
(25, 'AalbertsIPS', 'aalbertsips', 'AalbertsIPS', 'AalbertsIPS-1614345358.png', NULL, 'file', 'AalbertsIPS-1614345358.png', NULL, NULL, 23, '0000-00-00 00:00:00'),
(26, 'AalbertsIPS endorsement wSubBrands 3', 'aalbertsips-endorsement-wsubbrands-3', 'AalbertsIPS endorsement wSubBrands 3', 'AalbertsIPS endorsement wSubBrands 3-1614345505.png', NULL, 'file', 'AalbertsIPS endorsement wSubBrands 3-1614345505.png', NULL, NULL, 23, '0000-00-00 00:00:00'),
(27, 'AalbertsIPS endorsement wSubBrands 3 stacked', 'aalbertsips-endorsement-wsubbrands-3-stacked', 'AalbertsIPS endorsement wSubBrands 3 stacked', 'AalbertsIPS endorsement wSubBrands 3 stacked-1614345544.png', NULL, 'file', 'AalbertsIPS endorsement wSubBrands 3 stacked-1614345544.png', NULL, NULL, 23, '0000-00-00 00:00:00'),
(28, 'IPS keyvisual+textbox medium v2', 'ips-keyvisual-textbox-medium-v2', 'IPS keyvisual+textbox medium v2', 'IPS keyvisual textbox medium v2-1614345605.jpg', NULL, 'file', 'IPS keyvisual+textbox medium v2-1614345605.jpg', NULL, NULL, 23, '0000-00-00 00:00:00'),
(29, 'Apollo Valves', 'apollo-valves', 'Logo for Apollo Valves', 'Apollo Valves-1614345700.png', NULL, 'folder', NULL, NULL, NULL, 24, '0000-00-00 00:00:00'),
(30, 'EPC', 'epc', 'Logo for EPC', 'EPC-1614345742.png', '', 'folder', NULL, NULL, NULL, 24, '0000-00-00 00:00:00'),
(31, 'Shurjoint', 'shurjoint', 'Logo for Shurjoint', 'Shurjoint-1614345803.png', '', 'folder', NULL, NULL, NULL, 24, '0000-00-00 00:00:00'),
(84, 'Letterhead EPC 2020', 'letterhead-epc-2020', 'Letterhead EPC 2020', 'Letterhead EPC 2020-1614894912.png', NULL, 'file', 'Letterhead EPC 2020-1614894973.docx', NULL, NULL, 3, '0000-00-00 00:00:00'),
(85, 'Letterhead Shurjoint 2020 Matthews', 'letterhead-shurjoint-2020-matthews', 'Letterhead Shurjoint 2020 Matthews', 'Letterhead Shurjoint 2020 Matthews-1614894950.png', NULL, 'file', 'Letterhead Shurjoint 2020 Matthews-1614894950.docx', NULL, NULL, 3, '0000-00-00 00:00:00'),
(86, 'Letterhead ApolloValves 2020 Facilities', 'letterhead-apollovalves-2020-facilities', 'Letterhead ApolloValves 2020 Facilities', 'Letterhead ApolloValves 2020 Facilities-1614895096.png', NULL, 'file', 'Letterhead ApolloValves 2020 Facilities-1614895096.docx', NULL, NULL, 19, '0000-00-00 00:00:00'),
(87, 'Salesforce', 'salesforce', 'Training videos for Salesforce', 'Salesforce-1614894832.png', 'http://www.apollovalves.com/marketing/salesforce', 'folder', NULL, NULL, NULL, 14, '0000-00-00 00:00:00'),
(88, 'Business Cards', 'business-cards', 'Training video for business cards', 'Business Cards-1614895036.png', 'https://www.screencast.com/t/MucC9OtK', 'folder', NULL, NULL, NULL, 14, '0000-00-00 00:00:00'),
(89, 'Microsoft Teams Training', 'microsoft-teams-training', 'AIPS Microsoft Teams Training 2.5.21', NULL, NULL, 'video', NULL, 'https://youtu.be/BGUegm4WbWo', NULL, 14, '0000-00-00 00:00:00'),
(90, 'General employee internal.pp. 9.4.20', 'general-employee-internal-pp-9-4-20', 'General employee internal.pp. 9.4.20', 'General employee internal.pp. 9.4.20-1614895231.png', NULL, 'file', 'General employee internal.pp. 9.4.20-1614895231.pptx', NULL, NULL, 12, '0000-00-00 00:00:00'),
(91, 'Shurjoint cmyk', 'shurjoint-cmyk', 'Shurjoint cmyk', 'Shurjoint cmyk-1614896075.png', NULL, 'file', 'Shurjoint cmyk-1614896075.png', NULL, NULL, 31, '0000-00-00 00:00:00'),
(92, 'Shurjoint Gray', 'shurjoint-gray', 'Shurjoint Gray', 'Shurjoint Gray-1614896100.png', NULL, 'file', 'Shurjoint Gray-1614896100.png', NULL, NULL, 31, '0000-00-00 00:00:00'),
(93, 'ApolloPOWERPRESS Signature', 'apollopowerpress-signature', 'ApolloPOWERPRESS Signature', 'ApolloPOWERPRESS Signature-1614896118.png', NULL, 'file', 'ApolloPOWERPRESS Signature-1614896118.png', NULL, NULL, 29, '0000-00-00 00:00:00'),
(94, 'EPC Diamond cmyk', 'epc-diamond-cmyk', 'EPC Diamond cmyk', 'EPC Diamond cmyk-1614896146.png', NULL, 'file', 'EPC Diamond cmyk-1614896146.png', NULL, NULL, 30, '0000-00-00 00:00:00'),
(95, 'ApolloPRESS Signature', 'apollopress-signature', 'ApolloPRESS Signature', 'ApolloPRESS Signature-1614896147.png', NULL, 'file', 'ApolloPRESS Signature-1614896147.png', NULL, NULL, 29, '0000-00-00 00:00:00'),
(96, 'EPC Diamond Gray', 'epc-diamond-gray', 'EPC Diamond Gray', 'EPC Diamond Gray-1614896166.png', NULL, 'file', 'EPC Diamond Gray-1614896166.png', NULL, NULL, 30, '0000-00-00 00:00:00'),
(97, 'ApolloPUSH Signature', 'apollopush-signature', 'ApolloPUSH Signature', 'ApolloPUSH Signature-1614896179.png', NULL, 'file', 'ApolloPUSH Signature-1614896179.png', NULL, NULL, 29, '0000-00-00 00:00:00'),
(98, 'ApolloValves Retro OL cmyk', 'apollovalves-retro-ol-cmyk', 'ApolloValves Retro OL cmyk', 'ApolloValves Retro OL cmyk-1614896210.png', NULL, 'file', 'ApolloValves Retro OL cmyk-1614896210.png', NULL, NULL, 29, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ak_products`
--

DROP TABLE IF EXISTS `ak_products`;
CREATE TABLE IF NOT EXISTS `ak_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `limited_quantity` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `product_images` varchar(255) NOT NULL,
  `available_quantity` varchar(255) DEFAULT NULL,
  `product_details` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ak_products`
--

INSERT INTO `ak_products` (`id`, `title`, `description`, `limited_quantity`, `image`, `product_images`, `available_quantity`, `product_details`, `created_at`) VALUES
(1, 'Porduct title', 'Porduct Description', 'Limited Quantity', 'Advanced-1614362439.png', '', '10', '', '0000-00-00 00:00:00'),
(2, 'Product details', 'Product details desci', 'Limited Quantity', 'Product details-1619151719.jpg', '', '10', 'Product details', '2021-04-23 06:21:59'),
(25, 'Rerum omnis sed cons', 'Debitis iste consect', '', 'Rerum omnis sed cons-1620992618.jpg', '1620992618.h.jpg,1620992618.lalit.png,1620992618.ww.png', '260', NULL, '2021-05-14 07:53:41'),
(68, 'Sequi et amet volup', 'Qui doloribus sapien', '', 'Sequi et amet volup-1620992260.png', '1620992260.t.jpg', '829', NULL, '2021-05-14 13:37:40'),
(73, 'demo', 'example1234', '', 'demo-1620998539.png', '1620998539.lalit.png', '12', NULL, '2021-05-14 15:21:09'),
(74, 'Expedita corrupti d', 'Reiciendis tempora u', 'Limited Quantity', 'Expedita corrupti d-1621053683.jpg', '1621053683.bb.png,1621053683.h.jpg,1621053683.lalit.png,1621053683.Picture1.jpg,1621053683.Picture2.jpg,1621053683.ww.png', '805', NULL, '2021-05-15 06:41:23'),
(75, 'ddddd', 'dsvs', 'Limited Quantity', 'ddddd-1621065644.png', '1621065644.lalit.png,1621065644.t.jpg', '12', NULL, '2021-05-15 10:00:44'),
(76, 'fgfg', 'fhfghfgh', 'Limited Quantity', 'fgfg-1621065714.png', '1621065714.lalit.png,1621065714.t.jpg', '12', NULL, '2021-05-15 10:01:54'),
(77, 'sdfdf', 'sdfsdfsd', 'Limited Quantity', 'sdfdf-1621066006.png', '1621066006.h.jpg,1621066006.lalit.png,1621066006.t.jpg', '12', NULL, '2021-05-15 10:06:46');

-- --------------------------------------------------------

--
-- Table structure for table `ak_users`
--

DROP TABLE IF EXISTS `ak_users`;
CREATE TABLE IF NOT EXISTS `ak_users` (
  `user_title` varchar(5) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `additional_email` varchar(255) NOT NULL,
  `primery_contact` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` varchar(1) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `secondary_phone` varchar(255) NOT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `login_id` varchar(50) NOT NULL DEFAULT '',
  `login_password` varchar(50) NOT NULL,
  PRIMARY KEY (`login_id`),
  KEY `login_id` (`login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ak_users`
--

INSERT INTO `ak_users` (`user_title`, `f_name`, `l_name`, `email`, `additional_email`, `primery_contact`, `date_of_birth`, `gender`, `address`, `city`, `state`, `country`, `zipcode`, `secondary_phone`, `profile_pic`, `login_id`, `login_password`) VALUES
('Mr.', 'Raphael', 'Bennett', 'raphael.bennett@aalberts-ips.com', '', '9999999999', '1991-12-18', 'M', '-', '-', '-', '-', '-', '9999999999', '', 'raphael.bennett@aalberts-ips.com', 'Password01!!');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
