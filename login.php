<?php 
require_once('header.php');
?>
<section class="main_container breadcrumbs_main_container">
	<div class="container">
		<div class="row">
			<div class="col-12"> 
					<form method="POST" Class="form-horizontal login-form" action="" >
					

					<div class="form-group text-center">

						<img class="img-logo" width="90px" src="./images/svg-aalberts.png" />

					</div>

					<div class="form-group">

						<div class="col-md-12">

							<input type="text" placeholder="Enter your email" value="<?php if(isset($_POST['login-email'])){echo (empty($_POST['login-email']))?"":$_POST['login-email'];} ?>" class="form-control" name="login-email"/>

						</div>

					</div>

					<div class="form-group">

						<div class="col-md-12">

							<input type="password" placeholder="Enter Password" value="" class="form-control" name="login-pass"/>

						</div>

					</div>

					<div class="form-group">

						<div class="col-md-12 text-center">

							<input type="submit" name="signin" value="Sign in" class="btn btn-block btn-danger" />

						</div>

					</div>

					<?php 
						$reasons = array("empty" => "Email or passwords field empty. Try again?", "match" => "Email or passwords didn't match. Try again?"); 
						if (isset($_GET["loginFailed"]) && $_GET["loginFailed"]){ 
						echo '<p class="text-center text-danger error">'.$reasons[$_GET["reason"]].'</p>'; 
						}
					?>
					<p class="text-center"><a href="signIn.php" class="forgot_link">Don't Have Account?</a></p>
					<p class="text-center"><a href="forgot-password.php" class="forgot_link">Forgot Password?</a></p>

				</form>
			</div>
		</div>
	</div>
</section>

<?php 
include 'footer.php';
?>
