


<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script src="../js/datatables.js"></script>
<script type="text/javascript">

	$(document).ready(function(){
		function reset_form(){
			$('#data_submit_form input').val('');
		}
		$(location). attr("href");
		$("#data_submit_form").submit(function(e) {
	    e.preventDefault();
	    var form = $(this);
		var url = form.attr('action');

	    var formData = new FormData(this);
	    $.ajax({
	           	type:'POST',
	            url: url,
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	           success: function(data)
	           {
				   //$(".response_message").html(data); // show response from the php script.
				   console.log(data);
				   if(data == 1){
					  
		

					  $(".form_message_final").html("Records added successfully !");
					   $(".form_message_final").show();
					   setTimeout(function(){location.reload(true); },2000);
					  	//$(".response_message").html(data);
					  }else{
								  	//alert("Records updateded successfully !");
									  $(".form_message_final").html("Records updated successfully !");
					   $(".form_message_final").show();
					  setTimeout(function(){location.reload(true); },2000);
					  }
	           }
	         });


	});

		$(".plus_new_block a").click(function(){
			$("div#data_form_popup").show();
			/*$('#data_submit_form input[type="reset"]').trigger("click");*/
			$('#data_submit_form input[type="text"],#data_submit_form input[type="file"]').val('');
			$(".file_type_box").show();
			$(".response_message").html("");
			$("#form_action").val('add');
            $("h3.form_title").html('Add new product');

		});
		$("div#data_form_popup a.popup-close").click(function(){
			$("div#data_form_popup").hide();
		});
	});
</script>
<script type="text/javascript">
	$("document").ready(function(){
		$(".file_type").on("change click",function(){
			var check_val = $(this).val();
			if( check_val === 'folder'){
				$(".folder_display").show();
				$(".file_display").hide();
				$(".video_display").hide();
				$(".video_hide").show();
				$("#image_file").prop('required',false);
				$("#download_file").prop('required',false);
				$("#video_embed_url").prop('required',false);
			}else if(check_val === 'file'){
				$(".folder_display").hide();
				$(".file_display").show();
				$(".video_display").hide();
				$(".video_hide").show();
				$("#image_file").prop('required',false);
				$(".bg_image_required").hide();
				$("#download_file").prop('required',true);
				$("#video_embed_url").prop('required',false);
			}else{
				$(".folder_display").hide();
				$(".file_display").hide();
				$(".video_display").show();
				$(".video_hide").hide();
				$("#image_file").removeAttr("required");
				$("#download_file").prop('required',false);
				$("#video_embed_url").prop('required',true);
			}
		});
	});
</script>



<script type="text/javascript">
	   $(document).ready(function(){
            $('body').delegate('.delete_post','click',function(){
                var row_obj=$(this);
                var answer = confirm('Do you really want to delete this post?');
                if (answer){
                    var post_id=$(this).data("post_id");
                    $.post( "deleteProductAjax.php", { post_id: post_id})
					  .done(function( data ) {
					    if(data == "0"){
					    	alert("Post could not be deleted !");
							$(".form_message_final").html("Post could not be deleted !");
							$(".form_message_final").show();
							$(".form_message_final").css("background","#f00");
							setTimeout(function(){$(".form_message_final").hide(); },1000);
					    }else{
					    	//alert("Deleted Successfully.");
							$(".form_message_final").html("Deleted successfully !");
							$(".form_message_final").show();
							setTimeout(function(){$(".form_message_final").hide(); },1000);
					    	row_obj.parent("li").remove();
					    }
					  });

                }else{
                  return false;
                }
            });
            $('body').delegate('.edit_post','click',function(){
                var row_obj=$(this);
                var post_id = $(this).data("post_id");
                $("#form_action").val('edit');
                    $.post( "getProductAjax.php", { post_id: post_id})
					  .done(function( data ) {
					    if(data == "0"){
					    	alert("post ID is not correct !");
					    	/*alert("Post could not be deleted !");
							$(".form_message_final").html("Post could not be deleted !");
							$(".form_message_final").show();
							$(".form_message_final").css("background","#f00");
							setTimeout(function(){$(".form_message_final").hide(); },3000);*/
					    }else{
							data = $.parseJSON(data);
							console.log(data);
                            $("#title").val(data.title);
                            $("#description").val(data.description);
                            $("#product_details").val(data.product_details);
							$("#available_quantity").val(data.available_quantity);
							
                            $("#sort_order").val(data.sort_order);
                            $("#image_file").prop('required',false);
                            // $("#image_file").val(data.image);
                            $("#product_id").val(data.id);

                            $(".file_type_box").hide();
							$("div#data_form_popup .form_title").html("Edit Product");
							$("div#data_form_popup").show();
							$(".response_message").html("");
					    	/*//alert("Deleted Successfully.");
							$(".form_message_final").html("Deleted successfully !");
							$(".form_message_final").show();
							setTimeout(function(){$(".form_message_final").hide(); },3000);
					    	row_obj.parent("li").remove();*/
					    }
					  });
            });
        });



       $(".numeric").keypress(function (e) {
           //if the letter is not digit then display error and don't type anything
           if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               //display error message
               // $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
           }
       });
</script>



<script type="text/javascript">
	   $(document).ready(function(){
            $('body').delegate('.delete_post_product','click',function(){
                var row_obj=$(this);
                var answer = confirm('Do you really want to delete this product?');
                if (answer){
                    var post_id=$(this).data("post_id");
                    $.post( "deleteProductAjax.php", { post_id: post_id})
					  .done(function( data ) {
					    if(data == "0"){
					    	alert("Post could not be deleted !");
							$(".form_message_final").html("Post could not be deleted !");
							$(".form_message_final").show();
							$(".form_message_final").css("background","#f00");
							setTimeout(function(){$(".form_message_final").hide(); },1000);

					    }else{
					    	//alert("Deleted Successfully.");
							$(".form_message_final").html("Deleted successfully !");
							$(".form_message_final").show();
							setTimeout(function(){$(".form_message_final").hide(); },1000);
							row_obj.parent("li").remove();
							setTimeout(function(){location.reload(true); },2000);
					    }
					  });

                }else{
                  return false;
                }
            });
            $('body').delegate('.edit_post_product','click',function(){
                var row_obj=$(this);
				var post_id = $(this).data("post_id");
				
                $("#form_action").val('edit');
					$.post( "getProductAjax.php", { post_id: post_id})
					
					  .done(function( data ) {
						
					    if(data == "0"){
					    	alert("post ID is not correct !");
					    	/*alert("Post could not be deleted !");
							$(".form_message_final").html("Post could not be deleted !");
							$(".form_message_final").show();
							$(".form_message_final").css("background","#f00");
							setTimeout(function(){$(".form_message_final").hide(); },3000);*/
					    }else{
							data = $.parseJSON(data);
							console.log(data);
                            $("#title").val(data.title);
                            $("#description").val(data.description);
                            $("#product_details").val(data.product_details);
							$("#available_quantity").val(data.available_quantity);
							
						   $("#limited_quantity").val(data.limited_quantity);
						   $("#out_of_stock").val(data.out_of_stock);
						   


						   if(data.limited_quantity ==="Limited Quantity")
						   {
								$("#limited_quantity").attr("checked",true);
													
						   }
						   else{
								$("#limited_quantity").val("Limited Quantity");
						   }

						   if(data.out_of_stock ==="Out Of Stock")
						   {
								$("#outofstock").attr("checked",true);
													
						   }
						   else{
								$("#outofstock").val("Out Of Stock");
						   }
						   //alert(data.image);
							//$("#image_file").prop('required',false);
							
                            //$("#image_file").val(data.image);
                            $("#product_id").val(data.id);

                            $(".file_type_box").hide();
							$("div#data_form_popup .form_title").html("Edit Product");
							$("div#data_form_popup").show();
							$(".response_message").html("");
					    	/*//alert("Deleted Successfully.");
							$(".form_message_final").html("Deleted successfully !");
							$(".form_message_final").show();
							setTimeout(function(){$(".form_message_final").hide(); },3000);
					    	row_obj.parent("li").remove();*/
					    }
					  });
            });
        });



       $(".numeric").keypress(function (e) {
           //if the letter is not digit then display error and don't type anything
           if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               //display error message
               // $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
           }
       });
</script>

</body>


</html>
