<?php 

include '../inc/functions.php';
/*print_r($_REQUEST);
	print_r($_FILES);

exit;*/
if($_POST['action'] == 'add'){
	if($_POST['entry_type'] == 'folder'){
		$entry_type = trim($_POST['entry_type']);
		$title = trim($_POST['title']);
		$description = trim($_POST['description']);
		$ext_url = trim($_POST['ext_url']);
		$sort_order = trim($_POST['sort_order']);
		$parent_id = trim($_POST['parent_id']);
		/*print_r($_FILES);*/
		$target_dir="../upload-new/";
		$image = '';	
		if(isset($_FILES['image_file']) && !empty($_FILES['image_file']['name'])){
			$target_file=$target_dir.basename($_FILES['image_file']['name']);
			$filetype=pathinfo("$target_file",PATHINFO_EXTENSION);
			if($filetype=='jpg' || $filetype=='png' || $filetype=='tiff' || $filetype=='giff' || $filetype=='bmp'){
				//$f_name=basename($_FILES['image_file']['name']);
				$f_name=$title.'-'.time().'.'.$filetype;
				$target_file=$target_dir.$f_name;
				if($file=move_uploaded_file($_FILES['image_file']['tmp_name'], $target_file)){
					$image=trim(mysqli_real_escape_string($con,$f_name));
					insert_folder($title,$description,$image,$ext_url,$parent_id,$sort_order);
					//echo 'Image Upload Successfully. click <a></a>';
					/*insert_image($image,$user_data['login_id']);
					header("LOCATION: index.php");*/
					echo 1;
				}else{
					echo "Not Upload";
				}
			}
			else{
				echo "<p>Select image file and upload.example : .jpg/.png/.tiff/.giff/.bmp</p>";
			}
		}else{
			insert_folder($title,$description,$image,$ext_url,$parent_id);
			echo 1;
		}

	}elseif($_POST['entry_type'] == 'video'){
			$entry_type = trim($_POST['entry_type']);
			$title = trim($_POST['title']);
			$description = trim($_POST['description']);
			$video_embed_url = trim($_POST['video_embed_url']);
			$sort_order = trim($_POST['sort_order']);
			$parent_id = trim($_POST['parent_id']);
			if(insert_video($title,$description,$video_embed_url,$parent_id,$sort_order)){
				echo 1;
			}
	}elseif($_POST['entry_type'] == 'file'){

		$entry_type = trim($_POST['entry_type']);
		$title = trim($_POST['title']);
		$description = trim($_POST['description']);
		$ext_url = trim($_POST['ext_url']);
		$sort_order = trim($_POST['sort_order']);
		$parent_id = trim($_POST['parent_id']);
		/*print_r($_FILES);*/
		$target_dir="../upload-new/";
		$image_file = '';$error = 0;$error_msg = '';	$download_file = '';
		if(isset($_FILES['image_file']) && !empty($_FILES['image_file']['name'])){
			$target_file=$target_dir.basename($_FILES['image_file']['name']);
			$filetype=pathinfo("$target_file",PATHINFO_EXTENSION);
			if($filetype=='jpg' || $filetype=='png' || $filetype=='tiff' || $filetype=='giff' || $filetype=='bmp'){
				//$f_name=basename($_FILES['image_file']['name']);
				$f_name=$title.'-'.time().'.'.$filetype;
				$target_file=$target_dir.$f_name;
				if($file=move_uploaded_file($_FILES['image_file']['tmp_name'], $target_file)){
					$image_file=trim(mysqli_real_escape_string($con,$f_name));
				}else{
					$error_msg = "Not Upload";
					$error = 1;
				}
			}
			else{
				$error_msg = "<p>Select image file and upload.example : .jpg/.png/.tiff/.giff/.bmp</p>";
				$error = 1;
			}
		}
		if(isset($_FILES['download_file'])){
			$target_file=$target_dir.basename($_FILES['download_file']['name']);
			$filetype=pathinfo("$target_file",PATHINFO_EXTENSION);
			if($filetype=='pdf' || $filetype=='doc' || $filetype=='txt' || $filetype=='docx' || $filetype=='ppt' || $filetype=='pptx' || $filetype=='jpg' || $filetype=='png' || $filetype=='tiff' || $filetype=='giff' || $filetype=='bmp' || $filetype=='xls' || $filetype=='xlsx'){
				//$f_name=basename($_FILES['image_file']['name']);
				$f_name=$title.'-'.time().'.'.$filetype;
				$target_file=$target_dir.$f_name;
				if($file=move_uploaded_file($_FILES['download_file']['tmp_name'], $target_file)){
					$download_file=trim(mysqli_real_escape_string($con,$f_name));
				}else{
					$error_msg = "Not Upload";
					$error = 1;
				}
			}
			else{
				$error_msg = '<p class="text-danger">Select valid file type. Example : .pdf/.doc/.docx/.pptx/.ppt/.jpg/.png/.tiff/.giff/.bmp</p>';
				$error = 1;
			}

			if($error){
				echo $error_msg;
			}else{
				insert_file($title,$description,$image_file,$download_file,$parent_id,$sort_order);
				echo 1;
			}

		}
		
	}
}
if($_POST['action'] == 'edit'){
	/*print_r($_POST);*/
	if($_POST['entry_type'] == 'folder'){
		$entry_type = trim($_POST['entry_type']);
		$title = trim($_POST['title']);
		$description = trim($_POST['description']);
		$ext_url = trim($_POST['ext_url']);
		$parent_id = trim($_POST['parent_id']);
		$sort_order = trim($_POST['sort_order']);
		$post_id = trim($_POST['post_id']);
		/*print_r($_FILES);*/
		$target_dir="../upload-new/";
			
		if(isset($_FILES['image_file']) && !empty($_FILES['image_file']['name'])){
			$target_file=$target_dir.basename($_FILES['image_file']['name']);
			$filetype=pathinfo("$target_file",PATHINFO_EXTENSION);
			if($filetype=='jpg' || $filetype=='png' || $filetype=='tiff' || $filetype=='giff' || $filetype=='bmp'){
				//$f_name=basename($_FILES['image_file']['name']);
				$f_name=$title.'-'.time().'.'.$filetype;
				$target_file=$target_dir.$f_name;
				if($file=move_uploaded_file($_FILES['image_file']['tmp_name'], $target_file)){
					$image=trim(mysqli_real_escape_string($con,$f_name));
					update_folder($title,$description,$image,$ext_url,$parent_id,$post_id,$sort_order);
					//echo 'Image Upload Successfully. click <a></a>';
					/*insert_image($image,$user_data['login_id']);
					header("LOCATION: index.php");*/
					echo 1;
				}else{
					echo "Not Upload";
				}
			}
			else{
				echo "<p>Select image file and upload.example : .jpg/.png/.tiff/.giff/.bmp</p>";
			}
		}else{
			$post_detail = get_post_by_id($post_id);
			$image = $post_detail['post_image'];
			update_folder($title,$description,$image,$ext_url,$parent_id,$post_id,$sort_order);
			echo 1;
		}

	}elseif($_POST['entry_type'] == 'video'){
			$entry_type = trim($_POST['entry_type']);
			$title = trim($_POST['title']);
			$description = trim($_POST['description']);
			$video_embed_url = trim($_POST['video_embed_url']);
			$parent_id = trim($_POST['parent_id']);
			$sort_order = trim($_POST['sort_order']);
			$post_id = trim($_POST['post_id']);
			if(update_video($title,$description,$video_embed_url,$parent_id,$post_id,$sort_order)){
				echo 1;
			}else{
				echo 'Update failed !';
			}
	}elseif($_POST['entry_type'] == 'file'){

		$entry_type = trim($_POST['entry_type']);
		$title = trim($_POST['title']);
		$description = trim($_POST['description']);
		$ext_url = trim($_POST['ext_url']);
		$parent_id = trim($_POST['parent_id']);
		$sort_order = trim($_POST['sort_order']);
		$post_id = trim($_POST['post_id']);
		/*print_r($_FILES);*/
		$target_dir="../upload-new/";
		$image_file = '';$error = 0;$error_msg = '';	$download_file = '';
		if(isset($_FILES['image_file']) && !empty($_FILES['image_file']['name'])){
			$target_file=$target_dir.basename($_FILES['image_file']['name']);
			$filetype=pathinfo("$target_file",PATHINFO_EXTENSION);
			if($filetype=='jpg' || $filetype=='png' || $filetype=='tiff' || $filetype=='giff' || $filetype=='bmp'){
				//$f_name=basename($_FILES['image_file']['name']);
				$f_name=$title.'-'.time().'.'.$filetype;
				$target_file=$target_dir.$f_name;
				if($file=move_uploaded_file($_FILES['image_file']['tmp_name'], $target_file)){
					$image_file=trim(mysqli_real_escape_string($con,$f_name));
				}else{
					$error_msg = "Not Upload";
					$error = 1;
				}
			}
			else{
				$error_msg = "<p>Select image file and upload.example : .jpg/.png/.tiff/.giff/.bmp</p>";
				$error = 1;
			}
		}else{
			$post_detail = get_post_by_id($post_id);
			$image_file = $post_detail['post_image'];
		}
		if(isset($_FILES['download_file']) && !empty($_FILES['download_file']['name'])){
			$target_file=$target_dir.basename($_FILES['download_file']['name']);
			$filetype=pathinfo("$target_file",PATHINFO_EXTENSION);
			if($filetype=='pdf' || $filetype=='doc' || $filetype=='txt' || $filetype=='docx' || $filetype=='ppt' || $filetype=='pptx' || $filetype=='jpg' || $filetype=='png' || $filetype=='tiff' || $filetype=='giff' || $filetype=='bmp'){
				//$f_name=basename($_FILES['image_file']['name']);
				$f_name=$title.'-'.time().'.'.$filetype;
				$target_file=$target_dir.$f_name;
				if($file=move_uploaded_file($_FILES['download_file']['tmp_name'], $target_file)){
					$download_file=trim(mysqli_real_escape_string($con,$f_name));
				}else{
					$error_msg = "Not Upload";
					$error = 1;
				}
			}
			else{
				$error_msg = '<p class="text-danger">Select valid file type. Example : .pdf/.doc/.docx/.pptx/.ppt/.jpg/.png/.tiff/.giff/.bmp</p>';
				$error = 1;
			}
		}else{
			$post_detail = get_post_by_id($post_id);
			$download_file = $post_detail['download_file'];
		}
		if($error){
			echo $error_msg;
		}else{
			update_file($title,$description,$image_file,$download_file,$parent_id,$post_id,$sort_order);
			echo 1;
		}
		
	}
}

die();

?>