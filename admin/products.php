<?php
	require_once('header_inner.php');
	
?>

<section class="main_container">
	<div class="container">
		<div class="row">
			<div class="col-12"> 
				<div class="notification_section">
					<div class="notification_header">
						<!-- <h2><a href="#">notifications</a></h2> -->
						<h2><a href="#">Products</a></h2>
					</div>
					<!-- <ul class="notification_list">
						<li>
							<a href="#">
								<div class="news-left">
									<h2>purchasing Gotham font sets</h2>
									<p>An update of the brand guidelines is now available at the brandhub. This version of the guidelines s...</p>
								</div>
								<div class="news-right">
									Tue 23 Oct
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="news-left">
									<h2>purchasing Gotham font sets</h2>
									<p>An update of the brand guidelines is now available at the brandhub. This version of the guidelines s...</p>
								</div>
								<div class="news-right">
									Tue 23 Oct
								</div>
							</a>
						</li>
					</ul> -->
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
				<div class="plus_new_block">
						<a href="javascript:void(0);">
							<i class="far fa-plus-square"></i>
							<p>Click on above to add new product.</p>
						</a>

				</div>
<!--			--><?php
					$all_products = get_products();
//					echo '<pre>';
//					print_r($all_products); exit;

					/*print_r($folders);*/
					if(count($all_products)){
						foreach ($all_products as $product) {
						echo '<div class="col-lg-4 col-md-6 col-sm-6 col-12 mt-4 position-relative">';
							$url = '../product-details.php?pid='.$product['id'];
							echo '<a href="javascript:void(0);" class="delete_post_product" data-post_id="'.$product['id'].'"><i class="fas fa-trash"></i></a>
									<a href="'.$url.'"  style="text-decoration: none;"><div class="ProductSets h-100">
										<div class="ProImg">';
										if(!empty($product['image'])){
											echo '<img src="../upload-new/'.$product['image'].'"/>';
										}
										echo '</div>
										<div class="ProDetails p-3 pt-5">
											
											<p>'.$product['description'].'</p>
											<p class="brand_title requests">'.$product['title'].'</p>
										</div>
										</div>
									</a>
									<a data-post_id="'.$product['id'].'" class="edit_post_product" href="javascript:void(0);"><i class="fas fa-edit"></i></a>
								';
						echo '</div>';
					}
					}

				?>
		</div>
	</div>
</section>	


<?php
	require_once("product_add.php");

	require_once("product_footer.php");

	
	
?>
