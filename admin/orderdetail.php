<?php
require_once('header_inner.php');

?>

<section class="main_container cartTable">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="notification_section p-3">
					<div class="notification_header">
						<h2>Order Detail</h2>
					</div>
					<table class="orderDetails w-100">
						<tr>
							<td class="w-50">
								<h4 class="text-center">Order ID :<span><?php echo $_REQUEST['oid'] ?></span></h4>
							</td>
							<td class="w-50">
								<h4 class="text-center">Name :<span><?php echo $_REQUEST['pname'] ?></span></h4>
							</td>
						</tr>
					</table>

					<!-- <ul class="notification_list">
						<li>
							<a href="#">
								<div class="news-left">
									<h2>purchasing Gotham font sets</h2>
									<p>An update of the brand guidelines is now available at the brandhub. This version of the guidelines s...</p>
								</div>
								<div class="news-right">
									Tue 23 Oct
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="news-left">
									<h2>purchasing Gotham font sets</h2>
									<p>An update of the brand guidelines is now available at the brandhub. This version of the guidelines s...</p>
								</div>
								<div class="news-right">
									Tue 23 Oct
								</div>
							</a>
						</li>
					</ul> -->

					<br>
					<?php
					$sql = "SELECT * FROM ak_orders where order_id=" . $_REQUEST['oid'];
					
					$result = mysqli_query($con, $sql);

					if ($result->num_rows > 0) {
						echo "<table class='vid table table-striped table-bordered w-100' id='orderdetails'>";
						echo "<thead><tr><th class='text-center'>Product ID</th><th>Product Name</th><th>Product Image</th><th class='text-center'>Quantity</th></tr></thead><tbody>";
						while ($row = $result->fetch_assoc()) {

							$sql1 = "SELECT title,image FROM ak_products where id=" . $row['products_id'];
								
							$resp = mysqli_query($con, $sql1);
							$rw = mysqli_fetch_row($resp);
							//echo  $rw[1];
							echo '<tr >
							<td class="text-center">' . $row["products_id"] . '</a></td>
							
							
							<td>' . $rw[0] . '</td>
							<td><img src="../upload-new/' . $rw[1] . '" /></td>
							<td class="text-center">' . $row["qty"] . '</td>

							</tr>';
						}
						echo "</tbody></table>";
					} else {
						echo "0 results";
					}

					?>


				</div>
				<br>
			</div>
		</div>
	</div>

</section>


<?php
require_once("product_add.php");

require_once("product_footer.php");

?>

<script>
    $(document).ready(function() {
        $('#orderdetails').DataTable();
    });
</script>