<footer>
	<div class="container">
		<div class="row">
			<div class="col-12 footer_top">
			<a href="index.php"><img class="img-fluid logo-image" src="../images/logo.png" /></a>
			</div>
			<div class="col-6">
				<!--<ul class="footer_menu">
					<li><a href="#">brands</a></li>
					<li><a href="#">my profile</a></li>
				</ul> -->
			</div>
			<div class="col-6 footer_bottom">
				<p>For Questions: <a href="mailto:raphael.bennett@aalberts-ips.com">raphael.bennett@aalberts-ips.com</a></p>
			</div>
		</div>
	</div>

</footer>
<div class="form_message_final">Record added successfully !</div>
<div id="data_form_popup" class="popup_block" style="display: none;">
	<div class="popup_inner">
		<a href="javascript:void(0);" class="popup-close">+</a>
		<h3 class="form_title" class="text-center">Add new file or folder</h3>
		<form id="data_submit_form" action="submit_ajax.php" method="post" enctype="multipart/form-data">
			<input type="hidden" name="parent_id" value="<?php echo (isset($_REQUEST['pid'])?$_REQUEST['pid']:0);?>">
			<input id="form_action" type="hidden" name="action" value="add">
			<input id="post_id" type="hidden" name="post_id" value="0">
			  <div class="form-group row">
				  <label for="entry_type" class="col-sm-4 col-form-label">Choose Type <span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
			    	<div class="form-check form-check-inline file_type_box">
					  <input checked class="form-check-input file_type" required type="radio" name="entry_type" id="inlineRadio1" value="file">
					  <label class="form-check-label" for="inlineRadio1">File</label>
					</div>
					<div class="form-check form-check-inline file_type_box">
					  <input class="form-check-input file_type" required type="radio" name="entry_type" id="inlineRadio2" value="folder">
					  <label class="form-check-label" for="inlineRadio2">Folder</label>
					</div>
					<div class="form-check form-check-inline file_type_box">
					  <input class="form-check-input file_type" required type="radio" name="entry_type" id="inlineRadio3" value="video">
					  <label class="form-check-label" for="inlineRadio3">Youtube Video</label>
					</div>
			    </div>
			  </div>
			  <div class="form-group row ">
			    <label for="title" class="col-sm-4 col-form-label">Title <span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
			      <input type="text" name="title" required placeholder="Title" class="form-control" id="title" />
			    </div>
			  </div>
			  <div class="form-group row ">
			    <label for="description" class="col-sm-4 col-form-label">Description <span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
			      <input type="text" name="description" required placeholder="Description" class="form-control" id="description" />
			    </div>
			  </div>
			  <div class="form-group row video_hide">
			    <label for="" class="col-sm-4 col-form-label">Background Image</label>
			    <div class="col-sm-8">
			      <input type="file" name="image_file" class="form-control" id="image_file" />
			    </div>
			  </div>
			  <div style="display: none;" class="form-group row folder_display" >
			    <label for="title" class="col-sm-4 col-form-label">External URL</label>
			    <div class="col-sm-8">
			      <input type="text" placeholder="http://example.com/abc" name="ext_url" class="form-control" id="ext_url" />
			    </div>
			  </div>
			<div style="display: none;" class="form-group row video_display" >
			    <label for="title" class="col-sm-4 col-form-label">Video Embeded URL <span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
			      <input type="text" placeholder="http://www.youtube.com/embed/abcd" name="video_embed_url" class="form-control" id="video_embed_url" />
			    </div>
			  </div>
			  <div class="form-group row file_display">
			    <label for="" class="col-sm-4 col-form-label">Upload File <span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
			      <input type="file"   name="download_file" class="form-control" id="download_file" />
			    </div>
			  </div>
			  <div class="form-group row" >
			    <label for="title" class="col-sm-4 col-form-label">Sort Order</label>
			    <div class="col-sm-8">
			      <input type="text" placeholder="Sort Order" name="sort_order" class="form-control" id="sort_order" />
			    </div>
			  </div>
			  <div class="form-group row">
			    <div class="col-sm-6 text-center">
			      <!--<input type="submit" name="add_data" value="Save Data" class="btn btn-danger" /> -->
					<button name="add_data" type="submit" class="btn btn-danger" >Save Data</button>
			    </div>
			    <div class="col-sm-6 text-center">
			      <button type="button" onclick="$('#data_submit_form input[type=text],#data_submit_form input[type=file]').val('');" class="btn btn-danger" >Reset</button>
			    </div>
			  </div>

			  <div class="response_message"></div>
		</form> 
	</div>
</div>

