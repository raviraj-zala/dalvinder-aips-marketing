<?php 
session_start(); /* Starts the session */

if(isset($_SESSION['user'])){
	header("location:dashboard.php");
	exit;
}
require_once('../inc/functions.php');
if(isset($_POST['signin'])){
	$login_mail=trim($_POST['login-email']);
	$login_pass=trim($_POST['login-pass']);
	if(empty($login_mail) || empty($login_pass)){
		die(header("Location:index.php?loginFailed=true&reason=empty"));
	}
	else if(validate_id($login_mail,$login_pass)){
		$_SESSION['user']=$login_mail;
		header('Location:dashboard.php');
	}else{
		die(header("Location:index.php?loginFailed=true&reason=match"));
	}
}	
?>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Aalberts :: Integrated Piping Systems</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="icon" type="image/icon" href="../favicon.ico" sizes="96x115">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
 	<link rel="stylesheet" href="../css/bootstrap.min.css">
 	<link rel="stylesheet" href="../css/fontawesome-all.css">
 	<link rel="stylesheet" href="../css/style.css">


</head>
<body>

