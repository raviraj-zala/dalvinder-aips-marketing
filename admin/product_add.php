<footer>
	<div class="container">
		<div class="row">
			<div class="col-12 footer_top">
			<a href="index.php"><img class="img-fluid logo-image" src="../images/logo.png" /></a>
			</div>
			<div class="col-6">
				<!--<ul class="footer_menu">
					<li><a href="#">brands</a></li>
					<li><a href="#">my profile</a></li>
				</ul> -->
			</div>
			<div class="col-6 footer_bottom">
				<p>For Questions: <a href="mailto:raphael.bennett@aalberts-ips.com">raphael.bennett@aalberts-ips.com</a></p>
			</div>
		</div>
	</div>

</footer>
<div class="form_message_final">Record added successfully !</div>
<div id="data_form_popup" class="popup_block" style="display: none;">
	<div class="popup_inner">
		<a href="javascript:void(0);" class="popup-close">+</a>
		<h3 class="form_title">Add new Product</h3>
		<form id="data_submit_form" action="submit_product.php" method="post" enctype="multipart/form-data">
			<input id="form_action" type="hidden" name="action" value="add">
            <input id="product_id" type="hidden" name="product_id" value="0">

			  <div class="form-group row ">
			    <label for="title" class="col-sm-4 col-form-label">Title <span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
			      <input type="text" name="title" required placeholder="Title" class="form-control" id="title" />
			    </div>
			  </div>
			  <div class="form-group row ">
			    <label for="description" class="col-sm-4 col-form-label">Description <span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
                    <textarea  name="description" required placeholder="Description" class="form-control" id="description" ></textarea>
<!--                    <textarea type="text"/></div>-->
			    </div>
			  </div>
			
			  <div class="form-group row ">
			    <label for="product_details" class="col-sm-4 col-form-label">Limited Quantity </label>
			    <div class="col-sm-8">
                    <!-- <textarea  name="product_details" required placeholder="Product Details" class="form-control" id="product_details" ></textarea> -->
<!--                    <textarea type="text"/></div>-->
                    <input type="checkbox" name="limited_quantity" value="Limited Quantity" id="limited_quantity">
			    </div>
			  </div>
			  <div class="form-group row ">
			    <label for="available_quantity" class="col-sm-4 col-form-label">Total Stock Available<span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
                    <input type="number" name="available_quantity" required placeholder="Available Quantity" class="form-control numeric" id="available_quantity" />

<!--                    <inpu  name="available_quantity" required placeholder="Available Quantity" class="form-control" id="available_quantity" >-->
<!--                    <textarea type="text"/></div>-->
			    </div>
			  </div>

			  <div class="form-group row ">
			    <label for="product_details" class="col-sm-4 col-form-label">Is Out Of Stock? </label>
			    <div class="col-sm-8">
                    <!-- <textarea  name="product_details" required placeholder="Product Details" class="form-control" id="product_details" ></textarea> -->
<!--                    <textarea type="text"/></div>-->
                    <input type="checkbox" name="outofstock" value="Out Of Stock" id="outofstock">
			    </div>
			  </div>

			  <div class="form-group row file_display">
			    <label for="" class="col-sm-4 col-form-label">Main product Image<span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
			      <input type="file"  name="image_file"  class="form-control" id="image_file" />
			    </div>
			  </div>

			  <div class="form-group row file_display">
			    <label for="" class="col-sm-4 col-form-label">Product Images<span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
			      <input type="file"  name="product_images[]"  class="form-control" id="product_images[]" multiple/>
			    </div>
			  </div>





<!--			  <div class="form-group row" >-->
<!--			    <label for="title" class="col-sm-4 col-form-label">Sort Order</label>-->
<!--			    <div class="col-sm-8">-->
<!--			      <input type="text" placeholder="Sort Order" name="sort_order" class="form-control" id="sort_order" />-->
<!--			    </div>-->
<!--			  </div>-->
			  <div class="form-group row">
			    <div class="col-sm-12 text-center">
			      <!--<input type="submit" name="add_data" value="Save Data" class="btn btn-danger" /> -->
					<button name="add_data" type="submit" class="btn btn-danger" >Save Data</button>
					<button type="button" onclick="$('#data_submit_form input[type=text],#data_submit_form input[type=file]').val('');" class="btn btn-danger" >Reset</button>
			    </div>
			    <div class="col-sm-6 text-center">
			     
			    </div>
			  </div>

			  <div class="response_message"></div>
		</form> 
	</div>
</div>

