<footer>
	<div class="container">
		<div class="row">
			<div class="col-12 footer_top">
			<a href="index.php"><img class="img-fluid logo-image" src="../images/logo.png" /></a>
			</div>
			<div class="col-6">
				<!--<ul class="footer_menu">
					<li><a href="#">brands</a></li>
					<li><a href="#">my profile</a></li>
				</ul> -->
			</div>
			<div class="col-6 footer_bottom">
				<p>For Questions: <a href="mailto:raphael.bennett@aalberts-ips.com">raphael.bennett@aalberts-ips.com</a></p>
			</div>
		</div>
	</div>

</footer>
<div class="form_message_final">Record added successfully !</div>
<div id="data_form_popup" class="popup_block" style="display: none;">

	<div class="popup_inner">
	<h3 class="form_title">Add New Login Detail</h3>
		<a href="javascript:void(0);" class="popup-close">+</a>
		
		<form id="data_submit_form" action="submit_settings.php" method="post" enctype="multipart/form-data">
			<input id="form_action" type="hidden" name="action" value="add">
            <input id="product_id" type="hidden" name="settings_id" value="0">

			<div class="form-group row ">
			    <label for="name" class="col-sm-4 col-form-label">Name <span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
					<input type="text" name="name" required placeholder="Title" class="form-control" id="name" />
			    </div>
			  </div>

		  		<div class="form-group row ">
			    <label for="email" class="col-sm-4 col-form-label">E-mail <span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
					<input type="email" name="email" required placeholder="Title" class="form-control" id="email" />
			    </div>
			  </div>

			  <div class="form-group row ">
			    <label for="emt" class="col-sm-4 col-form-label">To E-mail <span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
					<input type="text" name="emt" pattern="^((\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)\s*[,]{0,1}\s*)+$"  required placeholder="Email" class="form-control" id="emt" />
			    </div>
			  </div>


			  <div class="form-group row ">
			    <label for="password" class="col-sm-4 col-form-label">Password<span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
					<input type="text" name="password" required placeholder="Title" class="form-control" id="password" />
			    </div>
			  </div>
			  <div class="form-group row ">
			    <label for="hname" class="col-sm-4 col-form-label">Host Name  <span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
			      <input type="text" name="hname" required placeholder="Title" class="form-control" id="hname" />
			    </div>
			  </div>
			  <div class="form-group row ">
			    <label for="ptype" class="col-sm-4 col-form-label"> Port Tye <span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
					<select name="ptype" required placeholder="Title" class="form-control" id="ptype" >
					<option value="ssl">ssl</option>
					<option value="tls">tls</option>
					<option value="SSL">SSL</option>
					<option value="TLS">TLS</option>
					</select>
			    </div>
			  </div>

			  <div class="form-group row ">
			    <label for="pno" class="col-sm-4 col-form-label"> Port Number <span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
					<input  type="number" name="pno" required placeholder="Title" class="form-control numeric" id="pno" />
			    </div>
			  </div>
			
		  <div class="form-group row">
			    <div class="col-sm-12 text-center">
			      <!--<input type="submit" name="add_data" value="Save Data" class="btn btn-danger" /> -->
					<button name="add_data" type="submit" class="btn btn-danger" >Save Data</button>
					<button type="button" onclick="$('#data_submit_form input[type=text],#data_submit_form input[type=file]').val('');" class="btn btn-danger" >Reset</button>
			    </div>
			    <div class="col-sm-6 text-center">
			     
			    </div>
			  </div>

			  <div class="response_message" javascript:void(0);></div>
		</form> 
	</div>
</div>
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>

<script type="text/javascript">
	   $(document).ready(function(){
            
        });
       $(".numeric").keypress(function (e) {
           //if the letter is not digit then display error and don't type anything
           if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               //display error message
               // $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
           }
       });
</script>