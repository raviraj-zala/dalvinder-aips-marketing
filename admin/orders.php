<?php
	require_once('header_inner.php');
	
?>

<section class="main_container cartTable">
	<div class="container">
		<div class="row">
			<div class="col-12"> 
				<div class="notification_section p-3">
					<div class="notification_header">
						 <h2><a href="#">List Of Orders</a></h2>
					
					</div>
					<!-- <ul class="notification_list">
						<li>
							<a href="#">
								<div class="news-left">
									<h2>purchasing Gotham font sets</h2>
									<p>An update of the brand guidelines is now available at the brandhub. This version of the guidelines s...</p>
								</div>
								<div class="news-right">
									Tue 23 Oct
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="news-left">
									<h2>purchasing Gotham font sets</h2>
									<p>An update of the brand guidelines is now available at the brandhub. This version of the guidelines s...</p>
								</div>
								<div class="news-right">
									Tue 23 Oct
								</div>
							</a>
						</li>
					</ul> -->

					<br>
					<?php



					$sql="SELECT * FROM ak_front_users ORDER BY id DESC";
					$result=mysqli_query($con,$sql);

				
					if ($result->num_rows > 0) {
						echo"<table class='vid table table-striped table-bordered w-100' id='listoforder'>";
						echo"<thead><tr><th class='text-center'>Order ID</th><th>Name</th><th>Email</th><th>Manager Email</th><th>Department</th><th>Customer</th><th>Event</th><th>Business Type</th><th>Sales Revenue</th><th>Shipping address</th><th>Needed Date</th><th>T-Shirt Size</th><th class='text-center'>Order Date</th></tr></thead><tbody>";
						while($row = $result->fetch_assoc()) {
							echo '<tr >
							<td class="text-center text-in"><a href="orderdetail.php?oid=' . $row["order_id"]. '&pname=' . $row["name"]. '">' . $row["order_id"]. '</a></td>
							<td>' . $row["name"]. '</td>
							<td>' . $row["email"]. '</td>
							<td>' . $row["memail"]. '</td>
							<td>' . $row["department"]. '</td>
							<td>' . $row["customer"]. '</td>
							<td>' . $row["event"]. '</td>
							<td>' . $row["business_type"]. '</td>
							<td>' . $row["exptsales"]. '</td>
							<td>' . $row["shipping_address"]. '</td>
							<td>' . $row["whenneed"]. '</td>
							<td>' . $row["tshirt_size"]. '</td>
							<td class="text-center">' . $row["created_at"]. '</td>
							</tr>';
						 
						}
						echo "</tbody></table>";
					  } else {
						echo "0 results";
					  }
					
				?>
		
				</div>
				<br>
			</div>
		</div>
	</div>

</section>	


<?php
	require_once("product_add.php");

	require_once("product_footer.php");
	
?>

<script>
  
	
	$(document).ready(function() {
    $('#listoforder').DataTable( {
		"ordering": false,
		"scrollX": true,
		"orderFixed": [ 11, 'asc' ]
		
    } );
} );
</script>
