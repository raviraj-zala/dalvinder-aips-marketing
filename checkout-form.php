<?php
require_once('header.php');
// session_start();
// <?php echo  $_SESSION["product_id"];
?>


<div class="container mt-5">
	<div class="row">
		<div class="col-md-12">
		<h3 class="form_title mb-5">Order Information</h3>
	    	<form id="data_submit_form" action="submit_user.php" onsubmit="return validateForm()"  method="post" enctype="multipart/form-data">
			<input id="form_action" type="hidden" name="action" value="add">
            <input id="product_id" type="hidden" name="product_id" value="">

			  <div class="form-group row ">
			    <label for="name" class="col-sm-4 col-form-label">Name <span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
			      <input type="text" name="name" required placeholder="Name" class="form-control" id="name" />
			    </div>
			  </div>

			  <div class="form-group row ">
			    <label for="email" class="col-sm-4 col-form-label">Email <span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
                    <input type="email" name="email" required placeholder="Email" class="form-control" id="email" />

			    </div>
			  </div>

			  <div class="form-group row ">
			    <label for="memail" class="col-sm-4 col-form-label">Manager Email <span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
                    <input type="email" name="memail" required placeholder="Manager Email" class="form-control" id="memail" />

			    </div>
			  </div>

			  <div class="form-group row ">
			    <label for="department" class="col-sm-4 col-form-label">Department<span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
                    <input type="text" name="department" required placeholder="Enter department" class="form-control " id="department" />

			    </div>
			  </div>

              <div class="form-group row ">
			    <label for="customer" class="col-sm-4 col-form-label">Customer<span style="color:#f00;"></span> </label>
			    <div class="col-sm-8">
                    <textarea name="customer"    class="form-control" id="customer" ></textarea>
			    </div>
			  </div>

			  
              <div class="form-group row ">
			    <label for="event" class="col-sm-4 col-form-label">Event<span style="color:#f00;"></span> </label>
			    <div class="col-sm-8">
                    <textarea name="event"  class="form-control" id="event" ></textarea>
			    </div>
			  </div>

              <div class="form-group row ">
			    <label for="business_type" class="col-sm-4 col-form-label">How will this drive business?<span style="color:#f00;">*</span> </label>
			    <div class="col-sm-8">
                     <textarea  name="business_type" required  class="form-control" id="business_type"></textarea>
			    </div>
			  </div>
                    
			  <div class="form-group row ">
			    <label for="exptsales" class="col-sm-4 col-form-label">Expected Sales Revenue/Increase in business<span style="color:#f00;">*</span> </label>
			    <div class="col-sm-8">
                    <input type="number" name="exptsales" required placeholder="Enter Sales Revenue in $" class="form-control numeric" id="exptsales" />

			    </div>
			  </div>

              <div class="form-group row ">
			    <label for="shipping_address" class="col-sm-4 col-form-label">Shipping Address<span style="color:#f00;">*</span> </label>
			    <div class="col-sm-8">
                     <textarea  name="shipping_address" required placeholder="Enter Address" class="form-control" id="shipping_address" ></textarea>
                    
			    </div>
			  </div>

			  <div class="form-group row ">
			    <label for="whenneend" class="col-sm-4 col-form-label">When do you need it by?<span style="color:#f00;">*</span> </label>
			    <div class="col-sm-8">
                     <input type="date"  name="whenneend" required placeholder="Enter Address" class="form-control" id="whenneend" onchange="TDate()" />
                    
			    </div>
			  </div>

			  <div class="form-group row ">
			    <label for="whenneend" class="col-sm-4 col-form-label">Enter T-Shirt Size <span style='font-size: 10px;'>If applicable</span> </label>
			    <div class="col-sm-8">
                     <input type="text" maxlength="200"  name="tshirtsize" placeholder="Enter T-Shirt Size" class="form-control" id="tshirtsize"/>
                    
			    </div>
			  </div>
			 
			  <div class="form-group row">
			    <div class="col-sm-12 text-center">
				<label class="col-sm-4 col-form-label" style="text-align: center; font-weight:bold;">Note : 10 Days lead time.</label><br><br>
			      <!--<input type="submit" name="add_data" value="Save Data" class="btn btn-danger" /> -->
					<button name="add_data" type="submit" class="btn btn-danger" >Submit</button>
					<button type="button" onclick="$('#data_submit_form input[type=text],#data_submit_form input[type=file]').val('');" class="btn btn-danger">Reset</button>
			    </div>
			    </div>
			    <div class="col-sm-6 text-center">
			   
			  </div>

			  <div class="response_message" javascript:void(0);></div>
		</form> 
		</div>
	</div>
</div>


<?php
	require_once("footer.php");
	
?>

<script type="text/javascript">
function validateForm() {
  var event = document.forms["data_submit_form"]["event"].value;
  var cust = document.forms["data_submit_form"]["customer"].value;
  
  if (event == "" && cust == "") {
	   alert('Enter one of Event or Customer ')
    return false;
  }
} 
</script>
<script type="text/javascript">
	   $(document).ready(function(){
		var ToDate = new Date();
	ToDate.setDate(ToDate.getDate() + 11);

	var dstr = ToDate.getFullYear() + "-" + (ToDate.getMonth()+1).toString().padStart(2,"0") + "-" + ToDate.getDate().toString().padStart(2,"0") ;

	document.getElementById("whenneend").setAttribute('min', dstr);
        });
       $(".numeric").keypress(function (e) {
           //if the letter is not digit then display error and don't type anything
           if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               //display error message
               // $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
           }
       });
</script>

<script type="text/javascript">
function TDate() {
    
	var UserDate = document.getElementById("whenneend").value;
    var ToDate = new Date();
	ToDate.setDate(ToDate.getDate() + 10);
	
    if (new Date(UserDate).getTime() > ToDate.getTime() ) {
         
          return true;
	 }
	 else
	 {
		//alert("Select delivery date only from 10 from now");
		document.getElementById("whenneend").value = ToDate;
		return false;
	 }
   
}
</script>
              
              

