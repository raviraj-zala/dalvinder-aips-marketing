<div class="form_message_final">Record added successfully !</div>
<div id="data_form_popup" class="popup_block"
     <?php if($_SERVER['REQUEST_METHOD'] != 'POST') {
         echo 'style="display: none;"';
     }
     ?>
>
	<div class="popup_inner">
<!--		<a href="javascript:void(0);" class="popup-close">+</a>-->
		<h3 class="form_title">Add your Details</h3>
	    	<form id="data_submit_form" action="submit_user.php" method="post" enctype="multipart/form-data">
			<input id="form_action" type="hidden" name="action" value="add">
            <input id="product_id" type="hidden" name="product_id" value="0">

			  <div class="form-group row ">
			    <label for="title" class="col-sm-4 col-form-label">Name <span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
			      <input type="text" name="name" required placeholder="Name" class="form-control" id="title" />
			    </div>
			  </div>
			  <div class="form-group row ">
			    <label for="description" class="col-sm-4 col-form-label">Email <span style="color:#f00;">*</span></label>
			    <div class="col-sm-8">
                    <input type="text" name="email" required placeholder="Email" class="form-control" id="email" />

			    </div>
			  </div>
			  <div class="form-group row ">
			    <label for="product_details" class="col-sm-4 col-form-label">Phone Number </label>
			    <div class="col-sm-8">
                    <input type="text" name="phone" placeholder="Phone Number" class="form-control numeric" id="title" />

			    </div>
			  </div>


			  <div class="form-group row">
			    <div class="col-sm-6 text-center">
			      <!--<input type="submit" name="add_data" value="Save Data" class="btn btn-danger" /> -->
					<button name="add_data" type="submit" class="btn btn-danger" >Submit</button>
			    </div>
			    <div class="col-sm-6 text-center">
			      <button type="button" onclick="$('#data_submit_form input[type=text],#data_submit_form input[type=file]').val('');" class="btn btn-danger" >Reset</button>
			    </div>
			  </div>

			  <div class="response_message"></div>
		</form> 
	</div>
</div>




<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script type="text/javascript">

    $(document).ready(function(){
        function reset_form(){
            $('#data_submit_form input').val('');
        }
        $(location). attr("href");
        $("#data_submit_form").submit(function(e) {
            e.preventDefault();

            var email = $('#email').val();

            $('#user_email').val(email);

            var form = $(this);
            var url = form.attr('action');
            var formData = new FormData(this);
            $.ajax({
                type:'POST',
                url: url,
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                success: function(data)
                {
                    //$(".response_message").html(data); // show response from the php script.
                    if(data == 1){
                        //alert("Records updateded successfully !");
                        $(".form_message_final").html("Records Saved successfully !");
                        $(".form_message_final").show();
                        $("#data_form_popup").hide();
                        // setTimeout(function(){
                        //     window.location.href = 'popup.php';
                        //     },2000);
                    }else{
                        $(".response_message").html(data);
                    }
                }
            });


        });

        $(".plus_new_block a").click(function(){
            $("div#data_form_popup").show();
            /*$('#data_submit_form input[type="reset"]').trigger("click");*/
            $('#data_submit_form input[type="text"],#data_submit_form input[type="file"]').val('');
            $(".file_type_box").show();
            $(".response_message").html("");
            $("#form_action").val('add');

        });
        $("div#data_form_popup a.popup-close").click(function(){
            $("div#data_form_popup").hide();
        });
    });
</script>
<script type="text/javascript">
    $("document").ready(function(){
        $(".file_type").on("change click",function(){
            var check_val = $(this).val();
            if( check_val === 'folder'){
                $(".folder_display").show();
                $(".file_display").hide();
                $(".video_display").hide();
                $(".video_hide").show();
                $("#image_file").prop('required',false);
                $("#download_file").prop('required',false);
                $("#video_embed_url").prop('required',false);
            }else if(check_val === 'file'){
                $(".folder_display").hide();
                $(".file_display").show();
                $(".video_display").hide();
                $(".video_hide").show();
                $("#image_file").prop('required',false);
                $(".bg_image_required").hide();
                $("#download_file").prop('required',true);
                $("#video_embed_url").prop('required',false);
            }else{
                $(".folder_display").hide();
                $(".file_display").hide();
                $(".video_display").show();
                $(".video_hide").hide();
                $("#image_file").removeAttr("required");
                $("#download_file").prop('required',false);
                $("#video_embed_url").prop('required',true);
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('body').delegate('.edit_post','click',function(){
            var row_obj=$(this);
            var post_id = $(this).data("post_id");
            $("#form_action").val('edit');
            $.post( "getProductAjax.php", { post_id: post_id})
                .done(function( data ) {
                    if(data == "0"){
                        alert("post ID is not correct !");
                        /*alert("Post could not be deleted !");
                        $(".form_message_final").html("Post could not be deleted !");
                        $(".form_message_final").show();
                        $(".form_message_final").css("background","#f00");
                        setTimeout(function(){$(".form_message_final").hide(); },3000);*/
                    }else{
                        data = $.parseJSON(data);
                        console.log(data);
                        $("#title").val(data.title);
                        $("#description").val(data.description);
                        $("#product_details").val(data.product_details);
                        $("#available_quantity").val(data.available_quantity);
                        $("#sort_order").val(data.sort_order);
                        $("#image_file").prop('required',false);
                        // $("#image_file").val(data.image);
                        $("#product_id").val(data.id);

                        $(".file_type_box").hide();
                        $("div#data_form_popup .form_title").html("Edit File / Folder");
                        $("div#data_form_popup").show();
                        $(".response_message").html("");
                        /*//alert("Deleted Successfully.");
                        $(".form_message_final").html("Deleted successfully !");
                        $(".form_message_final").show();
                        setTimeout(function(){$(".form_message_final").hide(); },3000);
                        row_obj.parent("li").remove();*/
                    }
                });
        });
    });



    $(".numeric").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            // $("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });
</script>
