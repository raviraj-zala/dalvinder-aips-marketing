<?php
require_once('header.php');

?>

<section class="main_container">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="notification_section">
                    <div class="notification_header">
                        <!-- <h2><a href="#">notifications</a></h2> -->
                        <h2><a href="#">Products Checkout</a></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
      
        <div class="row">
                <?php
                session_start();
                $product = $_POST['product_id'];
                // echo $product;
                // $_SESSION["product_id"]=$product;
                $all_products = get_product_by_id($product);
                /*echo '<pre>';
                print_r($all_posts);*/
                /*print_r($folders);*/
                if(count($all_products)){
                $post_available = 1;
                    //  foreach ($all_products as  $key => $all_product) {


                      echo '<div class="col-lg-4 col-md-6 col-sm-6 col-12 mt-4">';
                        $url = 'product-details.php?pid='.$all_products['id'];
                        echo '<a href="'.$url.'" target="_blank" style="text-decoration: none;"><div class="ProductSets h-100">
                            <div class="ProImg">';
                        if(!empty($all_products['image'])){
                            echo '<img src="upload-new/'.$all_products['image'].'"/>';
                        }
                        echo '</div>
										
                                        <div class="ProDetails p-3 pt-5">
											<h4>'.$all_products['title'].'</h4>
											<p>'.$all_products['description'].'</p>
										    <p class="brand_title requests">'.$all_products['title'].'</p>
										</div>
									</div>
								</a>';
                    echo '</div>';
                    // }
                }
                if(!$post_available){
                    echo '<p> No Product Found !</p>';
                }

                ?>

                <form method="post" action="checkout-form.php" >
                    <input name="product_id" type="hidden" value="<?= $_POST['product_id'];?>">
                    <input name="user_email" id="user_email" type="hidden">
                    <div class="row mt-5">
<!--                        <div class="col-xs-6" style="margin-left: 13px;">-->
<!--                            <div class="product_quantity"> <span>QTY: </span>-->
<!--                                <input id="quantity_input" name="quantity" type="text" pattern="[0-9]*" value="1">-->
<!--                                <div class="quantity_buttons">-->
<!--                                    <div id="quantity_inc_button" onclick="incrementInc()" class="quantity_inc quantity_control"><i class="fas fa-chevron-up"></i></div>-->
<!--                                    <div id="quantity_dec_button" onclick="decrementInc()" class="quantity_dec quantity_control"><i class="fas fa-chevron-down"></i></div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="col-xs-6">
                            <!--                                                    <button type="submit" class="btn btn-primary shop-button">Add to Cart</button>-->
                            <button type="submit" class="btn btn-danger shop-button">Check out</button>
                        </div>
                    </div>
                </form>
        </div>
    </div>

</section>


<?php

    // require_once("popup.php");
    require_once("footer.php");
?>