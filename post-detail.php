                                                                  <?php
require_once('header1.php');



	$post_detail = $parent_details = '';
	$all_posts = get_posts_by_parent();
	if(isset($_REQUEST['pid']) && $_REQUEST['pid']>0){
		$all_posts = get_posts_by_parent($_REQUEST['pid']);
		$post_detail = get_post_by_id($_REQUEST['pid']);
		$parent_details = get_post_by_id($post_detail['parent_id']);
	}
					$post_available = 0;
					$folders = array();
					$files = array();
					$videos = array();
					/*echo '<pre>';
					print_r($all_posts);*/
					foreach ($all_posts as $post) {
						if($post['post_type'] == 'folder'){
							$folders[] = $post;
						}
						if($post['post_type'] == 'file'){
							$files[] = $post;
						}
						if($post['post_type'] == 'video'){
							$videos[] = $post;
						}
					}
	/*print_r($folders);*/
?>
<section class="main_container breadcrumbs_main_container">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="notification_section breadcrumbs_box">
					<ul class="breadcrumbs">
						<li><a href="index.php">Home</a> <i class="fas fa-chevron-right"></i></li>
						<?php
							if(isset($parent_details['id']) && $parent_details['id']>0){
								echo '<li><a href="post-detail.php?pid='.$parent_details['id'].'">'.$parent_details['post_title'].'</a> <i class="fas fa-chevron-right"></i></li>';
							}
						?>
						<li><?=$post_detail['post_title'];?></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
<!-- 	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="notification_section">
					<div class="notification_header">
						<h2><a href="#">notifications</a></h2>
					</div>
					<ul class="notification_list">
						<li>
							<a href="#">
								<div class="news-left">
									<h2>purchasing Gotham font sets</h2>
									<p>An update of the brand guidelines is now available at the brandhub. This version of the guidelines s...</p>
								</div>
								<div class="news-right">
									Tue 23 Oct
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="news-left">
									<h2>purchasing Gotham font sets</h2>
									<p>An update of the brand guidelines is now available at the brandhub. This version of the guidelines s...</p>
								</div>
								<div class="news-right">
									Tue 23 Oct
								</div>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div> -->
	<div class="container">
		<div class="row">

			<div class="col-12 pad0">
			<?php

					if(count($folders)){
						$post_available = 1;
						echo '<ul class="brand_list">';
						foreach ($folders as $folder) {
							$url = 'post-detail.php?pid='.$folder['id'];
							$target = "";
							if(isset($folder['external_url']) && $folder['external_url'] != '' ){
								$url = $folder['external_url'];
								$target = 'target="_blank"';
							}
							echo '<li>
									<a '.$target.' href="'.$url.'">
										<div class="list-image">';
										if(!empty($folder['post_image'])){
											echo '<img src="upload-new/'.$folder['post_image'].'"/>';
										}
										echo '</div>
										<div class="brand_hover_box">
											<h4>'.$folder['post_title'].'</h4>
											<p>'.$folder['post_description'].'</p>
										</div>
										<div class="brand_title">'.$folder['post_title'].'</div>
									</a>
								</li>';
						}
						echo '</div>';
					}
					if(count($files)){
						$post_available = 1;
						echo '<ul class="brand_list inner_list">';
						foreach ($files as $file) {
							echo '<li>
						<div class="list_inner_block">
							<a href="#">
								<div class="list-image">';
										if(!empty($file['post_image'])){
											echo '<img src="upload-new/'.$file['post_image'].'"/>';
										}
										echo '</div>
								<div class="brand_detail">
									<p>'.$file['post_title'].'</p>
								</div>
							</a>
							<a class="download_link" download href="upload-new/'.$file['download_file'].'"><svg id="download" viewBox="0 0 11 10"><path data-name="Download icon" d="M10.39 5.4a.6.6 0 0 0-.61.6v2.81H1.22V6a.6.6 0 0 0-.61-.6A.61.61 0 0 0 0 6v3.4a.61.61 0 0 0 .61.6h9.78a.6.6 0 0 0 .61-.6V6a.6.6 0 0 0-.61-.6zM5.26 7.38a.313.313 0 0 0 .24.12.3.3 0 0 0 .24-.12l2-2.68a.271.271 0 0 0 .02-.31.308.308 0 0 0-.26-.16H6.2V.68a.7.7 0 0 0-1.4 0v3.55H3.5a.308.308 0 0 0-.26.16.29.29 0 0 0 .02.31z"></path></svg> Download</a>
						</div>
					</li>';
						}
						echo '</div>';
					}
					if(count($videos)){
						$post_available = 1;
						echo '<ul class="brand_list inner_list videos_list">';
						foreach ($videos as $video) {
							echo '<li>
								<div class="list_inner_block">
										<div class="list-videos">
											<iframe width="100%" height="250" src="https://www.youtube.com/embed/'.get_youtube_id($video['video_embed_url']).'" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
										</div>
										<div class="video_detail">
											<h3>'.$video['post_title'].'</h3>
											<p>'.$video['post_description'].'</p>
										</div>
								</div>
							</li>';
						}
						echo '</ul>';
					}
					if(!$post_available){
						echo '<p> No record found !</p>';
					}
				?>
			</div>
		</div>
	</div>
</section>


<?php

	require_once("footer.php");
?>
