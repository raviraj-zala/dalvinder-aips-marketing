<?php
include 'inc/functions.php';

?>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Aalberts :: Integrated Piping Systems</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicon -->
	<link rel="icon" type="image/icon" href="favicon.ico" sizes="96x115">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/fontawesome-all.css">
	<link rel="stylesheet" href="css/datatables.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/table.css">
	<link rel="stylesheet" href="css/popup-style.css">
	<link rel="stylesheet" href="css/product-style.css">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-H9NEY738PZ"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-H9NEY738PZ');
	</script>

</head>

<body>
	<header class="main-header">
		<div class="container">
			<div class="row">
				<div class="col-3 left_header">
					<a href="index.php"><img class="img-fluid logo-image" src="images/logo.png" /></a>
				</div>
				
				<div class="col-9 right_header">
			
					<div class="main_menu"><i class="fas fa-bars"></i><span>menu</span>
						<ul class="menu_drop">

							<li><a href="products.php"><i class="fab fa-product-hunt"></i>Marketing Store </a></li>
							<!--
						<li><a href="viewcart.php"><i class="fas fa-shopping-cart"></i> 
						Cart <?php
								if (!isset($_SESSION['tq'])) {
									echo "<br>Total Item : 0";
								} else {
									echo "<br>Total Item : " . $_SESSION['tq'];
								}


								?> </a></li> -->
							<li><a href="admin"><i class="fas fa-user"></i> Login</a></li>

							<!-- <li><a href="logout.php"><i class="fas fa-arrow-right"></i> logout</a></li> -->
						</ul>
					</div>
				</div>
			</div>
		</div>

	</header>
