<?php
require_once('header.php');





$post_detail = $parent_details = '';
// print_r($_REQUEST['product_id']);exit();
if (isset($_REQUEST['pid']) && $_REQUEST['pid'] > 0) {
    $product = get_product_by_id($_REQUEST['pid']);
    
	// print_r($product);
    // exit();
   
}
$post_available = 0;

/*print_r($folders);*/
?>

<section class="main_container breadcrumbs_main_container">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="notification_section breadcrumbs_box bg-bg-transparent p-0">
                    <ul class="breadcrumbs">
                        <li><a href="index.php">Home</a> <i class="fas fa-chevron-right"></i></li>
                        <li><a href="products.php">Marketing Store</a> <i class="fas fa-chevron-right"></i></li>
                        <li><?php echo $product['title']; ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    

    <div class="container">
        <div class="product-details">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="product-gallery">
                    <div id="img_preview"><img id="img_preview1" src="<?= 'upload-new/' . $product['image']; ?>" alt="" style="border:solid 2px red" ></div>
                        
                    </div>
                </div>

              

                <div class="col-lg-6 col-md-12 col-sm-12 col-12 mt-3 mt-lg-0">
                    <div class="product-text">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="product-names mt-0"><?= $product['title']; ?></h1>
                            </div>
                            <div class="col-md-12">
                                <p class="product-short-desc"><?= $product['description']; ?></p>
                            </div>
                        </div>
                        
                        <form method="post" action="manage-cart.php">
                            <input name="product_id" type="hidden" value="<?= $product['id']; ?>">
                            <input type="hidden" name="cnt" value="1">
                            <input name="pdp" type="hidden" value="pdp">

                            <?php
                            $outofstock = $product['out_of_stock'];
                            if($outofstock=='Out Of Stock')
                            {
                                echo $outofstock;
                            }
                            else
                            {
                                echo '
                                <div class="row">
                                    <div class="col-md-12 mt-3 mb-3">
                                        <div class="product_quantity"> <span>QTY: </span>
                                        
                                            <input id="quantity_input" name="quantity" type="text" pattern="[0-9]*" value="1">
                                           
                                            <div class="quantity_buttons">
                                                <div id="quantity_inc_button" onclick="incrementInc()" class="quantity_inc quantity_control"><i class="fas fa-chevron-up"></i></div>
                                                <div id="quantity_dec_button" onclick="decrementInc()" class="quantity_dec quantity_control"><i class="fas fa-chevron-down"></i></div>
                                            </div>
                                            
                                        </div>
                                        
                                    </div>
                                   <div class="d-sm-flex position-relative">
                                  ';
                                            $product_quantity = $product['available_quantity'];
    
                                               if ($product_quantity <= 0) {
                                                    echo "<p class='outStock'>Out of Stock</p>";
                                                }
                                                else{
                                                    echo "<p class='inStock'>In Stock</p>";
                                                }
                                               echo ' <p class="limitedQty"> '.$product['limited_quantity'].' </p>';    
                                    echo '           
                                       
                                   </div>
                                   
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-danger shop-button">Add to cart</button>
                                    </div>
                                </div>
                                ';
                            }
          
                            ?>
                            
                        </form>
                    </div>
                </div>
            </div>
            
            <?php $multi_images=$product['product_images'];
                $product_images=array(explode(",",$multi_images));
                //  print_r($product_images);
                //  exit();
                $i=0;
                
                  foreach($product_images[0] as $images)
                  { 
                      $i++;?>
                  <div class="col-md-12">
                    <div class="product-thumb" id="img-thumb"  >
                        <img id="test<?php echo $i; ?>" src="<?php echo 'upload-new/' . $images;  ?>"  onclick="showPicture(this);" class="pimg" name="pimg" alt="" >
                    </div>
                </div>
                     <?php
                  }
              ?>

<br>
<br>
<br>


            <div class="col-lg-12 col-md-12 col-sm-12 col-12 mt-5">
                <div class="ProHeading" >
                   
                    <p><?php 
                    
                    if($product['product_details'] == '')
                    {

                    }
                    else{
                        echo "<h2>Product Details</h2>";
                        $product['product_details']; 
                    }
                   
                    
                    
                    ?></p>
                </div>
            </div>


        </div>
    </div>
</section>

<script>
function showPicture(e) {
  var sourceOfPicture = e.src;
  var img = document.getElementById('img_preview1');
  img.src = sourceOfPicture;
  img.style.display = "block";
  $(".pimg").css("border", "0");
  document.getElementById(e.id).style.border = "3px solid red";
 } 
</script>
<script>
    function incrementInc() {
        var initital_val = $('#quantity_input').val();
        var checkValue = isNaN(initital_val) ? 0 : initital_val;
        checkValue++;
        $('#quantity_input').val(checkValue);
    }
    function decrementInc() {
        var initital_val = $('#quantity_input').val();

        if (initital_val > 0) {
            var checkValue = isNaN(initital_val) ? 0 : initital_val;
            checkValue--;
            $('#quantity_input').val(checkValue);
        }
    }
 $('.slider-for').slick({
   slidesToShow: 1,
   slidesToScroll: 1,
   arrows: false,
   fade: true,
   asNavFor: '.slider-nav'
 });
 $('.slider-nav').slick({
   slidesToShow: 5,
   slidesToScroll: 1,
   asNavFor: '.slider-for',
   dots: true,
   focusOnSelect: true
 });

 $('a[data-slide]').click(function(e) {
   e.preventDefault();
   var slideno = $(this).data('slide');
   $('.slider-nav').slick('slickGoTo', slideno - 1);
 });
</script>
<?php
require_once("footer.php");
?>